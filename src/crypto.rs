pub trait Substitution {
    fn substitute(&self, message: &str) -> String; 
}
pub trait Transposition {
    fn transpose(&self, message: String, password: &str) -> String;
}