pub mod crypto;

use crate::crypto::{ Substitution, Transposition };
use std::collections::{ HashMap };


#[derive(Debug)]
pub struct ADFGX<'a> {
    adfgx: HashMap<u8, &'a str>,
    grid_1: [[char; 5]; 5]
}

// 5x5 Matrix. Filled up from [0;0] with chars of the key.
// The remaining field are filled up with the letters
// of the alphabet in reverse 
// ie. password wikipedia > key['w', 'i', 'k', 'p', 'e', 'd', 'a']
// Matrix >
// row 0: ['w', 'i', 'k', 'p', 'e]
// row 1: ['d', 'a', 'z', 'y', 'x'] ..
// (z, y, x, v...)
const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

impl <'a>ADFGX<'a> {
    pub fn new(password: &str) -> Self {
        let pass_1 = password.to_lowercase().chars().collect::<Vec<char>>();
        let key = ADFGX::key_from_password(pass_1);
        std::mem::drop(password);
        let mut letters: Vec<char> = key; 
        let alphabet = ALPHABET.chars().rev().collect::<Vec<char>>();
        for a in alphabet {
            if a != 'j' {
                if !letters.contains(&a) {
                    letters.push(a);
                }
            }
        }

        let mut grid_1 = [['_'; 5]; 5];     
        for row in 0..5 {
            grid_1[row].copy_from_slice(&letters[(row * 5)..(row * 5) + 5]);
        }

        let mut adfgx = HashMap::<u8, &'a str >::new();

        adfgx.insert(0, "A");
        adfgx.insert(1, "D");
        adfgx.insert(2, "F");
        adfgx.insert(3, "G");
        adfgx.insert(4, "X");
        ADFGX { 
            adfgx,
            grid_1
        }
    }

    fn key_from_password(chars: Vec<char>) -> Vec<char> {
        let mut picked: HashMap<char, u8> = HashMap::new();
        
        for c in &chars { picked.insert(*c, 0_u8); }
        let mut key: Vec<char> = vec![];
        for c in chars {
            if let Some(v) = picked.get_mut(&c) {
                if v == &0u8 {
                    key.push(c);
                    *v = 1;
                }
            }
        }
        key
    }
}

impl <'a>Substitution for ADFGX<'a> {
    fn substitute(&self, message: &str) -> String {
        let mut encrypted = String::from("");
        for mut c in message.chars() {
            c = c.to_lowercase().to_string().remove(0);
            if c == 'j' { c = 'i'; }
            for u in 0..5 {
                for v in 0..5 {     
                    if self.grid_1[u][v] == c {
                        encrypted.push_str( &self.adfgx.get(&(u as u8) ).unwrap().to_string() ) ;
                        encrypted.push_str( &self.adfgx.get(&(v as u8) ).unwrap().to_string() ) ;
                    }     
                }
            }
        }
        encrypted
    }
}
// TODO: bug. crashes if password contains uppercase
struct EncryptedMessage { msg: String }

impl std::fmt::Display for EncryptedMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut fmtd = String::new();
        let mut count = 0;
        let mut col = 0;
        for c in self.msg.chars() {
            if col == 9 {
                fmtd.push_str("\n");
                col = 0;
            }
            if count == 4 {
                fmtd.push_str(" ");
                count = 0;
                col += 1;
            } else {
                fmtd.push_str(&c.to_string());
                count += 1;
            }
           
        }
        write!(f, "{}", fmtd)
    }
}

impl <'a>Transposition for ADFGX<'a> {
    fn transpose(&self, message: String, password: &str) -> String {
        let mut chars: Vec<char> = password.to_lowercase().chars().collect();
        chars.sort();
        let values: Vec<usize> = chars
            .iter()
            .enumerate()
            .fold(vec![0; password.len()], |mut values, (index, &ch)| {
                let idx = password
                    .chars()
                    .enumerate()
                    .position(|(pos, c)| c == ch && values[pos] == 0)
                    .unwrap();
               
                values[idx] = index + 1;
                values
            })
            .into_iter()
            .collect();
        
        let mut grid_2 = vec![Vec::<char>::new(); chars.len()];
        let mut slot = 0;
        for m in message.chars() {
            grid_2[slot].push(m);
            if slot <= grid_2.len() - 2 {
                slot += 1;
            } else {
                slot = 0;
            }
        }

        let mut encrypted_message = String::new();
        for v in grid_2 {
            encrypted_message.push_str( &v.into_iter().collect::<String>() );
        }
        let em = EncryptedMessage { msg: encrypted_message };
        em.to_string()
    }
}