extern crate itertools;
use plotters::prelude::*;
use itertools::{ Itertools };
use std::ops::Neg;
/***
1. On a 2 dimensional grid, a heart shape will be placed around the center (0,0)
2. {x = 16sin^3 (t), y = 13cos(t) - 5cos(2t) - 2cos(3t) - cos(4t)} 
3. The heart can be rotated or changed in size.
4. Only condition: Must always surround (0,0)
5. Key defines x, y 
6. x pending + - inside the x, like 90deg rotated sin(x)
6. Order (+x, +y), (+x, -y), (-x, -y), (-x, +y)
PICK
key         18446744073709551615
1. y=sin(1) ^-1
2. y=-sin(8) ^-8
3. y=-sin(-4) ^-4   
**/
enum Order { PosPos, PosNeg, NegNeg, NegPos }

// Any hashing algorithm
fn encrypt_key(passphrase: &str) -> String {
    let byte_pass = passphrase.as_bytes();
    let mut secret_key: String = String::new();
    for i in 0..byte_pass.len() - 2 {
        secret_key.push_str(&(byte_pass[i] as u16 | (byte_pass[i + 1] as u16) << 8).to_string());
    }
    drop(byte_pass);
    secret_key
}

fn coords(secret_key: String) -> Vec<(u8, u8)> {
    let mut v: Vec<u8> = vec![];
    for i in 0..secret_key.chars().collect::<Vec<char>>().len() - 4 {
        v.push( 
            secret_key[i..i + 2].parse().unwrap()
        )
    }
    drop(secret_key);
    // it's not guaranteed that the secret key is of even length.
    // if not last value popped.
    // if v.len() % 2 == 0 {
    //     println!("even");
    // } else {
    //     println!("odd");
    //     v.pop();
    // }


    v.iter()
        .step_by(2)
        .tuples::<(_,_)>()
        .map(|(m, n)| (*m,*n) )
        .collect_vec()
}

// 
fn quadrant(coords: Vec<(u8, u8)>) ->  Vec<(i32, i32)> {
    let mut q = 0;
    coords.iter()
        .map(|(x, y)| {
            let new = match q {
                0 => { q += 1; (  *x as i32,         *y as i32)         },
                1 => { q += 1; (  *x as i32,        (*y as i32).neg() ) },
                2 => { q += 1; ( (*x as i32).neg(), (*y as i32).neg() ) },
                3 => { q += 1; ( (*x as i32).neg(),  *y as i32)         }
                _ => { unreachable!()}
            };
          if q == 4 {
            q = 0
          }
          new
        }).collect()
}

// y / x or f(x) / x
fn slope(coord: (i32, i32)) -> f32 {
    coord.1 as f32 / coord.0 as f32
}
// {x = 16sin^3 (t), y = 13cos(t) - 5cos(2t) - 2cos(3t) - cos(4t)} HEART
// {x = 4sin^3 (t), y = 3cos(t) - 7cos(2t) - 4cos(3t) - cos(4t)}  BUTTERFLY
fn heart(t: u8, ord: (i8, i8)) -> (u8, u8) {
    let u = t as f64;
    let x = 16f64 * u.sinh().powf(3f64);
    let y = 13f64 * u.sinh() - 5f64 * (2f64 * u).cos() - 5f64 * u.cos() - 4f64 * u.cos();
    (
        (ord.0 as f64 * x).trunc() as u8, (ord.1 as f64 * y).trunc() as u8
    )
}

// fn plot(coords: Vec<(i32, i32)>) {
//     // canvas
//     let drawing_area = BitMapBackend::new("/home/genom/heart_curve.png", (600, 400))
//     .into_drawing_area();
//     drawing_area.fill(&WHITE).unwrap();
//     // grid + mesh

//     let mut ctx = ChartBuilder::on(&drawing_area)
//     // .set_label_area_size(LabelAreaPosition::Left, 40)
//     // .set_label_area_size(LabelAreaPosition::Bottom, 40)
//     .build_cartesian_2d(-100..100, -100..100)
//     .unwrap();

//     ctx.configure_mesh().draw().unwrap();
//     let mut chart = ChartBuilder::on(&drawing_area)
//     .build_cartesian_2d(-100..100, -100..100)
//     .unwrap();
 
//     // chart.draw_series(
//     //     LineSeries::new( coords.iter().map(|c| (c.0, c.1) ), &BLACK),
//     //     ).unwrap();
//     //Heart
  
//     let f_x: Vec<f64> = (-12..12).map(|t| {

//          (t as f64).sinh().powf(3f64) 
//     }).collect();
//     let f_y: Vec<f64> = (-15..15).map(|t| {
//         13f64 * (t as f64).cos() - 5f64 * 2f64 * (t as f64).cos() - 2f64 * 3f64 * (t as f64).cos() - 4f64 * (t as f64).cos() 
//     }).collect();


//     dbg!(coords.len());  

//     chart.draw_series(
//         LineSeries::new(
//             f_x.iter().enumerate().map(|(i, x)| {
//                 println!("{:?} {:?}", i, x);
//                 (
//                     (coords[i].0 as f64 * (i as f64).sinh().powf(3f64)  ) as i32,  // X
//                     (coords[i].1 as f64 * (i as f64).cos() - 5f64 * 2f64 * (i as f64).cos() - 2f64 * 3f64 * (i as f64).cos() - 4f64 * (i as f64).cos()) as i32    // Y
//                 )
//             }), &RED)
//     ).unwrap();

// }
fn main() -> Result<(), Box<dyn std::error::Error>> {
    // One party's passphrase of arbitrary length
    let passphrase = "kI8dn,:dz$gsi5(ju23Xn";

    dbg!(&passphrase);
    // An appropriate hashing algirithm generates the secret key
    let secret_key = encrypt_key(&passphrase);
    dbg!(&secret_key);
    // The secret key represent coordinates on the grid
    // by splitting the secret key into a vector of tuples
    // where each tuple represents a coordinate and each value
    // in the tuple represents either x or y
    // Example secret key: 284247526509..
    // split into length of four bits [ 2842, 4752, 6509]
    // Coords are: [ (28, 42), (47, 52), (65, 9)]
    let coords = coords(secret_key.to_string());
    // each quadrant will be visited clockwise
    // therefore the coords will be:
    // quad 1: coord (x, y) > coord (x, y)
    // quad 2: coord (x, y) > coord (x, -y)
    // quad 3: coord (x, y) > coord (-x, -y)
    // quad 4: coord (x, y) > coord (-x, y)
    let quad = quadrant(coords);
 //   dbg!(&quad);
    // next we find the slope of each 2 dimensional vector
    // on the grid from (0, 0) to coord[n] 
    // since the first coordinate is always (0,0)
    // the slope is y / x
    // for c in &quad {
    //     dbg!(slope(&c));
    // }
    // for the shared key, the coordinates of the point intersection
    // between slope f(x) = mx + 0
    // must be found
   // plot(quad);
   
    Ok(())
}