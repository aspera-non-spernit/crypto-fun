extern crate crypto_fun;
use crypto_fun::{ ADFGX, crypto::{ Substitution, Transposition } };

fn main() -> std::io::Result<()> {
    let mut args: Vec<String> = vec![];
    for a in std::env::args() {
        args.push(a);
    }

    let adfgx = ADFGX::new(&args[1]); // pass 1

    let sub = adfgx.substitute(&args[3]); // message
    println!("{}", adfgx.transpose(sub, &args[2]) ); // pass 2
    Ok(())
}
